<?php

$data = json_decode(file_get_contents("php://input"), 1);

if (isset($data["mail"]) && isset($data["img"])) {

    $img = str_replace("data:{$data["img_format"]};base64,", '', $data["img"]);

    $img = str_replace(' ', '+', $img);

    $random_hash = md5(date('r', time()));

    file_put_contents(__DIR__ . "/save_images/" . $random_hash . "." . $data["img_ext_format"], base64_decode($img));

    $to = $data["mail"];

    $subject = "Валентинка";

    $message = "<img src='{$data["img"]}' />";

    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=iso-8859-1';
    $headers[] = "To: <{$to}>";
    $headers[] = 'From: RTVI <valentine@rtvi.com>';
    $headers[] = 'Cc: valentine@rtvi.com';
    $headers[] = 'Bcc: valentine@rtvi.com';

    mail($to, $subject, $message, implode("\r\n", $headers));

    echo "ok";

    exit;

} else {

    echo "error";

    exit;
}