<?php

$data = json_decode(file_get_contents("php://input"), 1);

if (isset($data["img"])) {

    $img = str_replace("data:{$data["img_format"]};base64,", '', $data["img"]);

    $img = str_replace(' ', '+', $img);

    $random_hash = md5(date('r', time()));

    $local_path = "/save_images/" . $random_hash . "." . $data["img_ext_format"];

    file_put_contents(__DIR__ . $local_path, base64_decode($img));

    //Must be
    $social_links = [
        "facebook" => $local_path, // or exclude, for hide social button
        "telegram" => $local_path, // or exclude, for hide social button
        "twitter" => $local_path, // or exclude, for hide social button
        "viber" => $local_path, // or exclude, for hide social button
        "ok" => $local_path, // or exclude, for hide social button
        "vk" => $local_path, // or exclude, for hide social button
        "wh" => $local_path, // or exclude, for hide social button
    ];

    echo json_encode(["soc" => $social_links, "file" => $local_path]);
}

die(0);