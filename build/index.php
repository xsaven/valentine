<?php

header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Origin: *");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- page name-->
    <title>RTVI | Valentine</title>
    <!-- Required meta tags-->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="theme-color" content="#A03761"/>
    <!-- Optional styles-->
    <link rel="shortcut icon" href="s/images/useful/favicon.png" type="image/png"/>
<!--    <meta http-equiv="Access-Control-Allow-Origin" content="a2aef724.ngrok.io">-->
    <link href="s/css/all.css" rel="stylesheet"/>
    <script src="/ValentineCard.js"></script>
<!--    <script src="http://localhost:8080/build/ValentineCard.js"></script>-->
    <script>
        window.onload = function () { new ValentineCard("#Valentine-Card", {}); }
    </script>
</head>
<body>
<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg"
           xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <symbol id="icon-zoom-in" viewBox="0 0 475.1 475.1">
            <path fill="currentColor" stroke="none" d="M464.5,412.8l-97.9-97.9c23.6-34.1,35.4-72,35.4-113.9c0-27.2-5.3-53.2-15.9-78.1c-10.6-24.8-24.8-46.3-42.8-64.2 s-39.4-32.3-64.2-42.8C254.2,5.3,228.2,0,201,0c-27.2,0-53.2,5.3-78.1,15.8C98.1,26.4,76.7,40.7,58.7,58.7 c-18,18-32.3,39.4-42.8,64.2C5.3,147.8,0,173.8,0,201c0,27.2,5.3,53.2,15.8,78.1c10.6,24.8,24.8,46.2,42.8,64.2 c18,18,39.4,32.3,64.2,42.8c24.8,10.6,50.9,15.8,78.1,15.8c41.9,0,79.9-11.8,113.9-35.4l97.9,97.6c6.9,7.2,15.4,10.8,25.7,10.8 c10.1,0,18.7-3.6,25.8-10.7c7.1-7.1,10.7-15.7,10.7-25.8S471.6,419.9,464.5,412.8z M291.4,291.4c-25,25-55.1,37.5-90.4,37.5 c-35.2,0-65.3-12.5-90.4-37.5c-25-25-37.5-55.1-37.5-90.4c0-35.2,12.5-65.3,37.5-90.4c25-25,55.1-37.5,90.4-37.5 c35.2,0,65.3,12.5,90.4,37.5c25,25,37.5,55.1,37.5,90.4C328.9,236.2,316.4,266.3,291.4,291.4z M283.2,182.7h-64v-64 c0-2.5-0.9-4.6-2.7-6.4c-1.8-1.8-4-2.7-6.4-2.7h-18.3c-2.5,0-4.6,0.9-6.4,2.7c-1.8,1.8-2.7,3.9-2.7,6.4v64h-64 c-2.5,0-4.6,0.9-6.4,2.7c-1.8,1.8-2.7,3.9-2.7,6.4v18.3c0,2.5,0.9,4.6,2.7,6.4c1.8,1.8,3.9,2.7,6.4,2.7h64v64c0,2.5,0.9,4.6,2.7,6.4 c1.8,1.8,3.9,2.7,6.4,2.7h18.3c2.5,0,4.6-0.9,6.4-2.7c1.8-1.8,2.7-3.9,2.7-6.4v-64h64c2.5,0,4.6-0.9,6.4-2.7 c1.8-1.8,2.7-3.9,2.7-6.4v-18.3c0-2.5-0.9-4.6-2.7-6.4C287.9,183.6,285.7,182.7,283.2,182.7z"/>
        </symbol>
    </defs>
</svg>
<div class="l-wrapper">
    <header class="c-header js-header">
        <div class="container-fluid">
            <div class="c-header__left">
                <div class="c-header__logo"><a href="main_type_1.html">
                    <svg width="1em" height="1em" class="icon icon-logo ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-logo"></use>
                    </svg></a></div>
                <div class="c-header__info"><a href="#" class="info-btn">
                    <svg width="1em" height="1em" class="icon icon-info ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-info"></use>
                    </svg></a></div>
                <div class="c-header__live"><a href="live.html" class="c-live__on"><span>LIVE</span></a>
                </div>
                <div class="c-header__menu minimized">
                    <ul class="h-reset-list">
                        <li><a href="news.html">Новости</a></li>
                        <li><a href="infonoises.html">Инфошум</a></li>
                        <li><a href="lives.html">Эфир</a></li>
                        <li><a href="stories.html">Сюжеты</a></li>
                        <li><a href="#">Мнения</a></li>
                        <li><a href="#">Такое</a></li>
                        <li><a href="#">Тесты</a></li>
                    </ul>
                </div>
            </div>
            <div class="c-header__right">
                <div class="c-header__currency d-md-none d-lg-block">
                    <ul class="h-reset-list">
                        <li><span class="cur-key">
                    <svg width="1em" height="1em" class="icon icon-sheqel ">
                      <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-sheqel"></use>
                    </svg></span><span class="cur-val">167,39</span>
                        </li>
                        <li><span class="cur-key">
                    <svg width="1em" height="1em" class="icon icon-dollar ">
                      <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-dollar"></use>
                    </svg></span><span class="cur-val">59,39</span>
                        </li>
                        <li><span class="cur-key">
                    <svg width="1em" height="1em" class="icon icon-euro ">
                      <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-euro"></use>
                    </svg></span><span class="cur-val">70,02</span>
                        </li>
                        <li><span class="cur-key">
                    <svg width="1em" height="1em" class="icon icon-bitcoin ">
                      <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-bitcoin"></use>
                    </svg></span><span class="cur-val"><svg width="1em" height="1em" class="icon icon-dollar icon-dollar2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-dollar"></use></svg> 458 799</span>
                        </li>
                    </ul>
                </div>
                <div class="c-header__buttons">
                    <ul class="h-reset-list">
                        <li>
                            <button type="button" data-target-block=".c-header__search" class="c-header__search-trig button-toggle">
                                <svg width="1em" height="1em" class="icon icon-search ">
                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-search"></use>
                                </svg>
                            </button>
                        </li>
                        <li class="d-md-none">
                            <button type="button" data-target-block=".c-header__menu, .c-header__menu-close" class="c-header__menu-trig button-toggle">
                                <svg width="1em" height="1em" class="icon icon-menu ">
                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-menu"></use>
                                </svg>
                                <svg width="1em" height="1em" class="icon icon-cross ">
                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-cross"></use>
                                </svg>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="c-header__search minimized">
                <form>
                    <div class="form-footer minimized">
                        <button type="submit" class="c-header__search-submit">
                            <svg width="1em" height="1em" class="icon icon-search ">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-search"></use>
                            </svg><span>Поиск</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <div class="input-wrap">
                            <svg width="1em" height="1em" class="icon icon-search ">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-search"></use>
                            </svg>
                            <input type="search" name="search" placeholder="Поиск..." value="" required="required" class="form-control c-header__search-input"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </header><a href="#" data-target-block=".c-header__menu, .c-header__menu-close" class="c-header__menu-close button-hide minimized"></a>
    <main class="main--valentineTheme">
        <div class="valentine-mainWrapper">
            <div class="valentine-firstStep">
                <div class="valentine-title">
                    <div class="valentine-title__logo">
                        <svg width="1em" height="1em" class="icon icon-logo-white ">
                            <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-logo-white"></use>
                        </svg>
                    </div>
                    <h1>Отправить валентинку</h1>
                    <p>Даже если вы равнодушны к Новому году, для вас обязательно есть что почитать. <br/> И именно такую подборку мы для вас уготовили. Мы поговорим о любви, стрит-арте, затронем дискусионные темы и даже покажем концерт «Битлов»!</p>
                    <div class="valentine-title__button">
                        <button type="button">
                            <svg width="1em" height="1em" class="icon icon-heart3 ">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-heart3"></use>
                            </svg><span>Создать свою валентинку</span>
                        </button>
                    </div>
                </div>

            </div>

            <div id="Valentine-Card"></div>

        </div>
    </main>
    <div class="c-footer fadeIn">
        <div class="container">
            <div class="row">
                <div class="c-footer__col-1 col col-md-6 col-sm-12 col-xs-24">
                    <div class="c-footer__col-top">
                        <div class="c-footer__logo"><a href="main_type_1.html">
                            <svg width="1em" height="1em" class="icon icon-logo ">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-logo"></use>
                            </svg></a></div>
                        <div class="c-footer__copy"><span>© Все права защищены. <br/> Любое использование материалов допускается только с согласия редакции.</span></div>
                    </div>
                    <div class="c-footer__col-bottom">
                        <div class="c-footer__18plus"><span>18+</span></div><a href="#">Выходные данные СМИ RTVI</a>
                    </div>
                </div>
                <div class="c-footer__col-2 col col-md-6 col-sm-12 col-xs-24">
                    <div class="c-footer__col-top">
                        <div class="c-footer__ttl"><span>RTVI в социальных сетях</span></div>
                        <div class="c-footer__social">
                            <ul class="h-reset-list">
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-fb ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-fb"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-tw ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-tw"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-ok ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-ok"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-inst ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-inst"></use>
                                    </svg></a></li>
                            </ul>
                        </div>
                        <div class="c-footer__social">
                            <ul class="h-reset-list">
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-tg ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-tg"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-yt ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-yt"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-gl ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-gl"></use>
                                    </svg></a></li>
                                <li><a href="#">
                                    <svg width="1em" height="1em" class="icon icon-tg2 ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-tg2"></use>
                                    </svg></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="c-footer__col-bottom">
                        <div class="c-footer__txt"><span>Достойные новости <a href="#">@rtvichannel</a></span><span>Мы в <a href="#">Яндекс.Новостях</a>, <a href="#">Яндекс.Дзен </a>и <a href="#">Google.News </a></span></div>
                    </div>
                </div>
                <div class="c-footer__col-3 col col-md-6 col-sm-12 col-xs-24">
                    <div class="c-footer__col-top">
                        <div class="c-footer__ttl"><span>Подписка на рассылку</span></div>
                        <div class="c-footer__subscribe">
                            <form class="js-form-check">
                                <div class="form-group">
                                    <div class="input-wrap">
                                        <input type="text" name="subscribe" placeholder="Ваш Email..." value="" required="required" class="form-control"/>
                                    </div>
                                </div>
                                <button type="submit" class="subscribe-button"></button>
                            </form>
                        </div>
                    </div>
                    <div class="c-footer__col-bottom">
                        <div class="c-footer__txt"><a href="#">Как смотреть RTVI по обе стороны</a><a href="#">Команда RTVI</a><a href="#">Политика конфиденциальности</a></div>
                    </div>
                </div>
                <div class="c-footer__col-4 col col-md-6 col-sm-12 col-xs-24">
                    <div class="c-footer__col-top">
                        <div class="c-footer__ttl"> <span>Редакция</span>
                            <div class="form-group">
                                <div class="select-wrap">
                                    <select class="selectpicker">
                                        <option selected="selected">Москва</option>
                                        <option>Краснодар</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="c-footer__contacts"><span>115280, г. Москва, ул. Ленинская <br/>слобода, д. 26, этаж 3<br/>тел:<a href="tel:+7(499)579-86-96">+7(499)579-86-96</a></span></div>
                    </div>
                    <div class="c-footer__col-bottom">
                        <div class="c-footer__txt"><span>
                    Пресс-служба:
                     <a href="mailto:press@rtvi.com">press@rtvi.com</a></span><span>
                    Почта редакции:
                     <a href="mailto:news@rtvi.com">news@rtvi.com</a></span><span>
                    Реклама:
                     <a href="mailto:oalexandrova@rtvi.com">oalexandrova@rtvi.com</a></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>