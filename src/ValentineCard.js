const Vue = require('vue');
window.axios = require('axios');

class ValentineCard {

    constructor(selector, options = {}) {
        Vue.component('Layout', require("./Components/Layout.vue").default);

        this.app = new Vue({
            el: selector,
            data() {
                return {
                    options
                }
            },
            template: `<Layout :options="options"/>`
        });
    }
}

window.ValentineCard = ValentineCard;