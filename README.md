#Valentine

Основной пример в папке `/build`.
Для теста необходимо запустить в корне папки `build` `PHP Server` выполнив следующую 
комманду `php -S localhost:4532` ну и открываем в браузере `http://localhost:4532`.

#####Основной файл билда
```
/build/ValentineCard.js
```

#####Пример
```
/build/index.php
```

####Как использовать

Объявляем место инициализации
```html
<div id="Valentine-Card"></div>
```

Подключаем наш билд к `html`
```html
<script src="/ValentineCard.js"></script>
```

Инициализируем класс
```javascript
new ValentineCard("#Valentine-Card", {options});
```

####OPTIONS
Опции по умолчанию
```javascript
options = {
    covers_url: "/covers.json",
    characters_url: "/characters.json",
    save_handler_url: "/save.php",
    sender_handler_url: "/send.php",
    on_send_ok: function (response, obj) {
        
    },
    on_send_error: function (response, obj) {
        
    },
    img_format: "image/jpeg",
    from_who_placeholder: "Мария Ивановна",
    from_to_placeholder: "Любимой",
    icon_svg_path: "s/images/useful/svg/theme/symbol-defs.svg",
    repeat_confirm_text: "Вы уверены?"
}
```

#####covers_url
Путь по которому необходимо получить список Обложек
```
Пример формата: /build/covers.json
```
#####characters_url
Путь по которому необходимо получить список Содержимого
```
Пример формата: /build/characters.json
```
#####save_handler_url
Путь к скрипту, для сохранения изображения и получение ссылок к кнопкам шаринга
```
Пример: /build/save.php
```
#####sender_handler_url
Путь к скрипту, для отправки письма
```
Пример: /build/send.php
```
#####on_send_ok
`callback` функция при успешной отправки письма.
#####on_send_error
`callback` функция при не успешной отправки письма.
#####img_format
Формат в котором должно сохранятся изображение
```
По умолчанию: "image/jpeg"
Сохраняет: https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL
``` 
Если будет какой-то не стандартный формат, есть еще настройка `img_ext_format` с помощью которой вы укажите расширение файла в ручную.
#####from_who_placeholder
Информация после текста "От кого" на открытке по умолчанию
```
По умолчанию: "Мария Ивановна"
```  
#####from_to_placeholder
Информация после текста "Кому" на открытке по умолчанию
```
По умолчанию: "Любимой"
```  

#####icon_svg_path
Путь к `svg` иконкам
```
По умолчанию: "s/images/useful/svg/theme/symbol-defs.svg"
```

#####repeat_confirm_text
Текст при нажатии на кнопку повторить
```
По умолчанию: "Вы уверены?"
```
